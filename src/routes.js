import Home from './views/Home.vue'
import List from './views/List.vue'
import View from "./views/View.vue"

export default [
  {
    path: '/',
    component: Home
  },
  {
    path: '/articles',
    component: List
  },
  {
    path: '/article/:id',
    component: View
  }
]
